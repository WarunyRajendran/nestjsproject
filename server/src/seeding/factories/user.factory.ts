import { faker } from '@faker-js/faker';
import { define } from "typeorm-seeding";

import { UserEntity } from "../../user/user.entity";

define(UserEntity, () => {
  const user = new UserEntity();
  const firstName = faker.name.firstName();
  const lastName = faker.name.lastName();
  const email = faker.internet.email(`${firstName}`, `${lastName}`);
  const password = 'password';
  const userStatus = true;
  const skills = [
    "PHP", 
    "Javascript", 
    "Laravel",
    "HTML",
    "CSS",
    "React",
    "NestJs",
    "Symfony",
    "API Platform",
    "NodeJs"
  ]
  const city = faker.address.cityName();
  const birthDate = faker.date.birthdate({min: 18, max: 65, mode: 'age'});
  const hourlyRate = faker.datatype.number({min: 20, max: 100});
  
  const job = faker.name.jobType();
  const description = faker.lorem.paragraphs();
  const picture = faker.image.avatar();
  const role = [
    "ADMIN",
    "USER"
  ]

  user.firstName = `${firstName}`;
  user.lastName = `${lastName}`;
  user.password = `${password}`;
  user.email = `${email}`;
  user.status = userStatus;
  user.skills = skills;
  user.city = `${city}`;
  user.birthDate = birthDate;
  user.hourlyRate = hourlyRate;
  user.job = `${job}`;
  user.description = `${description}`;
  user.picture = `${picture}`;
  user.role = role;
  
  return user;
});