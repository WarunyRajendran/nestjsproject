import { faker } from '@faker-js/faker';
import { define } from "typeorm-seeding";

import { CategoryEntity } from '../../category/category.entity';

define(CategoryEntity, () => {
  const category = new CategoryEntity();
 
  const title = faker.word.noun();
  category.title = title;

  return category;
});