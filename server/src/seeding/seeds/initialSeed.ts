import { Factory, Seeder } from "typeorm-seeding";
import { Connection } from "typeorm";
import { faker } from '@faker-js/faker';

import { UserEntity } from "../../user/user.entity";
import { AssignmentEntity } from "../../assignment/assignment.entity";
import { CategoryEntity } from "../../category/category.entity";
import { MessageEntity } from "../../message/message.entity";
import { QuotationEntity } from "../../quotation/quotation.entity";
import { ReviewEntity } from "../../review/review.entity";

export default class InitialDatabaseSeed implements Seeder{
  public async run(factory: Factory, connection: Connection): Promise<void> {

    await factory(UserEntity)().createMany(15);
    await factory(CategoryEntity)().createMany(15);

    for (let i=0; i<15; i++){

      const assignment = new AssignmentEntity();
      const userRepo =  connection.getRepository(UserEntity);
      const randomFreelance = await userRepo
      .createQueryBuilder()
      .orderBy('RANDOM()')
      .take(1)
      .getOne();

      const randomCompany = await userRepo
      .createQueryBuilder()
      .orderBy('RANDOM()')
      .take(1)
      .getOne();

      const description = faker.lorem.paragraphs();
      const status = null;
      const paymentIntent = faker.datatype.string();

      assignment.description = `${description}`;
      assignment.status = status;
      assignment.paymentIntent = `${paymentIntent}`;
      assignment.freelance = randomFreelance;
      assignment.company = randomCompany;

      await connection.getRepository(AssignmentEntity).save(assignment);


      const categoryRepo = connection.getRepository(CategoryEntity);
      const randomCategory = await categoryRepo
      .createQueryBuilder()
      .orderBy('RANDOM()')
      .take(1)
      .getOne();
      
      await connection.createQueryBuilder()
      .relation(UserEntity, 'categories')
      .of(randomFreelance)
      .add(randomCategory);



      const message = new MessageEntity();
      const assigmentRepo = connection.getRepository(AssignmentEntity);
      const messageContent = faker.lorem.paragraphs();
      const randomAssignment =  await assigmentRepo
      .createQueryBuilder()
      .orderBy('RANDOM()')
      .take(1)
      .getOne();
      
      message.content = messageContent;
      message.assignment = randomAssignment;
      message.receiver = randomFreelance;
      message.transmitter = randomCompany;

      await connection.getRepository(MessageEntity).save(message);

      const quotation = new QuotationEntity();
      const amount = faker.datatype.number();
      const quotationStatus = null;
      const quotationDescription = faker.lorem.paragraphs();
      const nbDays = faker.datatype.number(); 

      quotation.amount = amount;
      quotation.status = quotationStatus;
      quotation.description = quotationDescription;
      quotation.nbDays = nbDays;
      quotation.freelance = randomFreelance;
      quotation.assignment = randomAssignment;

      await connection.getRepository(QuotationEntity).save(quotation);


      const review = new ReviewEntity();
      const reviewDescription = faker.lorem.paragraphs();
      const rate = faker.datatype.number({max: 5});
      
      review.description = reviewDescription;
      review.rate = rate;
      review.ratedUser = randomFreelance;
      review.ratingUser = randomCompany;

      await connection.getRepository(ReviewEntity).save(review);

    }
  }
} 
