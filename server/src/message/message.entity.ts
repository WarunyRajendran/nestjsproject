import { AssignmentEntity } from '../assignment/assignment.entity';
import { UserEntity } from '../user/user.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
} from 'typeorm';
  
  
@Entity('message')
export class MessageEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({
      type: 'text',
  })
  content: string;

  @ManyToOne(
    type => UserEntity,
    user => user.id,
  )
  transmitter: UserEntity;

  @ManyToOne(
    type => UserEntity,
    user => user.id,
  )
  receiver: UserEntity;

  @ManyToOne(
    type => AssignmentEntity,
    assignment => assignment.id,
  )
  assignment: AssignmentEntity;

}
  