import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  BeforeInsert,
  OneToMany,
  ManyToMany,
  JoinTable,
} from 'typeorm';

import { hash, compare } from 'bcryptjs';
import { sign } from 'jsonwebtoken';
import { TodoEntity } from '../todo/todo.entity';
import { UserSO } from './user.dto';
import { CategoryEntity } from '../category/category.entity';

@Entity('user')
export class UserEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({
    type: 'text',
    unique: true,
    nullable: true
  })
  email: string;

  @Column({
    type: 'text',
    nullable: true
  })
  password: string;

  @Column({
    type: String,
    length: 50,
    nullable: true
  })
  lastName: string;

  @Column({
    type: String,
    length: 50,
    nullable: true
  })
  firstName: string;

  @Column({
    type: String,
    length: 255,
    nullable: true
  })
  picture: string;

  @Column({
    type: 'boolean',
    nullable: true
  })
  status: boolean;

  @Column({
    type: 'json',
    nullable: true
  })
  skills: Array<string>;

  @Column({
    type: 'json',
  })
  role: Array<string>;

  @Column({
    type: String,
    length: 50,
    nullable: true
  })
  city: string;

  @Column({
    type: 'date',
    nullable: true
  })
  birthDate: Date;

  @Column({
    type: 'integer',
    nullable: true
  })
  hourlyRate: number;

  @Column({
    type: String,
    length: 50,
    nullable: true
  })
  job: string;

  @Column({
    type: 'text',
    nullable: true
  })
  description: string;

  @CreateDateColumn()
  createdOn: Date;

  @BeforeInsert()
  hashPassword = async () => {
    this.password = await hash(this.password, 8);
  };

  @OneToMany(
    type => TodoEntity,
    todo => todo.author,
  )
  todos: TodoEntity[];

  @ManyToMany(() => CategoryEntity)
  @JoinTable()
    categories: CategoryEntity

  comparePassword = async (attempt: string) => {
    return await compare(attempt, this.password);
  };

  sanitizeObject = (options?: SanitizeUserOptions): UserSO => {
    const { id, createdOn, email, token } = this;
    const responseObj = { id, createdOn, email };
    if (options?.withToken) {
      Object.assign(responseObj, { token });
    }
    return responseObj;
  };

  private get token() {
    const { id, email } = this;
    return sign(
      {
        id,
        email,
      },
      process.env.SECRET,
      { expiresIn: '3d' },
    );
  }
}

type SanitizeUserOptions = {
  withToken?: boolean;
};
