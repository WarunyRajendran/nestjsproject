import {
    Entity,
    PrimaryGeneratedColumn,
    Column
  } from 'typeorm';
  
  
  @Entity('category')
  export class CategoryEntity {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({
      type: String,
      length: 100
    })
    title: string;

}
  