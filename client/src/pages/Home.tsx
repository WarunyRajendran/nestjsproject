import React from 'react';
import { Box, Button, TextField } from '@material-ui/core';
import Footer from '../containers/Footer';
import '../styles/homePage/index.css';

const Home = () => {

  return (
    <Box className="home">
      <img src="./home/geometry1.svg" className="home--geometry1"/>

      <Box className="home--introduction">
        <h1>Netlink, la plateforme qui vous met en  <strong>relation</strong> avec des Freelances pour propulser vos projets.</h1>
        <img src="./home/team.avif" className="home--teamImg"/>
      </Box>

      <Box className="home--search">
        <h1 className="home--search--title--mobile">Rechercher un freelance</h1>
        <form className="home--search--form" noValidate autoComplete="off">
          <TextField id="outlined-basic" label="Votre recherche" variant="outlined" className=""/>
          <TextField id="outlined-basic" label="Lieu de la mission" variant="outlined" className=""/>
          <Button type="submit" variant="contained" className="">Trouver un freelance</Button>
        </form>
      </Box>

      <Box className="home--community">
        <h1 className="home--community--title">Netlink c'est avant tout une communauté</h1>
        <p className="home--community--accroche">C'est aussi collaborer en toute simplicité</p>

        <Box className="home--community--icons">
          <Box className="home--community--icons--boxes">
            <img src="./home/handshake.png" className="home--community--icons--hands"/>
            <p className="home--community--icons--title">50K entreprises </p>
            <p className="home--community--icons-desc">À la recherche de freelances</p>
          </Box>

          <Box className="home--community--icons--boxes">
            <img src="./home/rocket.png" className="home--community--icons--rocket"/>
            <p className="home--community--icons--title">+40K freelances</p>
            <p className="home--community--icons-desc">Aux multiples compétences</p>
          </Box>

          <Box className="home--community--icons--boxes">
            <img src="./home/conversation.png" className="home--community--icons--conv"/>
            <p className="home--community--icons--title">1 solution dédiée</p>
            <p className="home--community--icons-desc">Pensée et conçue pour collaborer</p>
          </Box>
        </Box>
      </Box>

      <Footer />
    </Box>
  );
};

export default Home;
