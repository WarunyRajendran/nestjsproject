import React from 'react';
import { Box, Typography, makeStyles } from '@material-ui/core';
import '../styles/footer.css';

const useStyles = makeStyles((theme) => ({
    appBar: {
      top: 'auto',
      bottom: 0,
    },
    toolbar: {
      alignItems: 'center',
      justifyContent: 'space-between',
    },
    footer: {
        backgroundColor: '#fffff',
        padding: theme.spacing(6),
        marginTop: '80px'
    },
}));

const Footer = () => {
  const classes = useStyles();

  return (
    <Box className={classes.footer}>
      <Typography variant="subtitle1" align="center" color="textSecondary">
        <a href="http://netlink.com" target="_blank" className="copyright">Copyright © 2023 Netlink.com - Tous droits réservés</a>
      </Typography>
    </Box>
  );
};

export default Footer;
