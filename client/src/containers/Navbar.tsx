import React, { useState } from 'react';
import { Button, makeStyles, Box, AppBar, Avatar, IconButton, Toolbar, Typography, TextField } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import MenuIcon from '@material-ui/icons/Menu';
import './navbar.css';

var classnames = require('classnames');

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  logo: {
    width: '90px',
    height: 'auto'
  },
  appBar: {
    backgroundColor: '#fff'
  },
  flexBox: {
    display: 'flex',
    width: '100%'
  },
  flex: {
    display: 'flex',
    width: '700px'
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  findFreelance: {
    width: '100%',
    marginTop: '90px',
    height: '200px',
    backgroundColor: '#ffff',
    textAlign: 'left',
    padding: '20px',
    opacity: 0,
    display: 'none',
  },
  searchBoxTitle: {
    display: 'block',
    margin: 'auto',
    width: '1200px'
  },
  paper: {
    marginRight: theme.spacing(2),
  },
  
}));

const Navbar = () => {
  const classes = useStyles();
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const anchorRef = React.useRef<HTMLButtonElement>(null);

  const [open, setOpen] = React.useState(false);
  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };

  const displaySearch = () => {
    setIsOpen(!isOpen);
    console.log(isOpen);
  };

  const handleClose = (event: React.MouseEvent<EventTarget>) => {
    if (anchorRef.current && anchorRef.current.contains(event.target as HTMLElement)) {
      return;
    }

    setOpen(false);
  };

  function handleListKeyDown(event: React.KeyboardEvent) {
    if (event.key === 'Tab') {
      event.preventDefault();
      setOpen(false);
    }
  }

  const prevOpen = React.useRef(open);
  React.useEffect(() => {
    if (prevOpen.current === true && open === false) {
      anchorRef.current!.focus();
    }

    prevOpen.current = open;
  }, [open]);

  return (
    <>
      <Box className="navbar">
        <Box flexGrow={1} >
          <AppBar  position={"fixed"} className={classes.appBar}>
              <Toolbar>
                <Box className="flexMobileMenu">
                  <Box className="menuMobile">
                    <div>
                      <IconButton 
                        edge="start" 
                        className={classes.paper} 
                        color="inherit" 
                        aria-label="menu"
                        ref={anchorRef}
                        aria-controls={open ? 'menu-list-grow' : undefined}
                        aria-haspopup="true"
                        onClick={handleToggle}
                      >
                        <MenuIcon />
                      </IconButton>
                      <Popper open={open} anchorEl={anchorRef.current} role={undefined} transition disablePortal>
                        {({ TransitionProps, placement }) => (
                          <Grow
                            {...TransitionProps}
                            style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
                          >
                            <Paper>
                              <ClickAwayListener onClickAway={handleClose}>
                                <MenuList autoFocusItem={open} id="menu-list-grow" onKeyDown={handleListKeyDown}>
                                  <MenuItem onClick={handleClose}>Explorer les profils</MenuItem>
                                  <MenuItem onClick={handleClose}>Inscription</MenuItem>
                                  <MenuItem onClick={handleClose}>Connexion</MenuItem>
                                </MenuList>
                              </ClickAwayListener>
                            </Paper>
                          </Grow>
                        )}
                      </Popper>
                    </div>
                  </Box>

                  <Box>
                    <Typography variant="h4" color="primary" className="mobileSiteName">Netlink</Typography>
                  </Box>

                  <Box>
                    <SearchIcon onClick={displaySearch}/>
                  </Box>
                </Box>

                <Box className={classes.flexBox} >
                  <Avatar src="./netlink.png" className={classes.logo}/>
                  <Button href="#">Explorer les profils</Button>
                </Box>
                  
                <Box className={classes.flex}>
                  <Button onClick={displaySearch} variant="outlined" className="desktopButtonSearch"><SearchIcon/>Trouver un freelance</Button>
                  <Button href="/register" variant="outlined" className="desktopButtonRegister">Inscription</Button>
                  <Button href="/login" style={{textDecoration: 'underline', fontWeight: 'bold'}}>Connexion</Button>
                </Box>
                
                <Box flexGrow={1}/>
              </Toolbar>
          </AppBar>
        </Box>
        <Box className={classnames('searchBox', {
            'searchBoxOpen' : isOpen
        })}>
          <Box className={classnames(classes.findFreelance, {
            'findFreelanceOpen' : isOpen
          })}>
            <Box className="findFreelance--title">
              <h1 style={{textAlign: 'left', marginLeft: '20px'}}>Vous avez des projets, nous avons des experts.</h1>
            </Box>
            <form className="findFreelanceForm" noValidate autoComplete="off">
              <TextField id="outlined-basic" label="Votre recherche" variant="outlined" className="findFreelanceForm--input"/>
              <TextField id="outlined-basic" label="Lieu de la mission" variant="outlined" className="findFreelanceForm--input"/>
              <Button type="submit" variant="contained" className="findFreelanceForm--submit">Trouver un freelance</Button>
            </form>
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default Navbar;

